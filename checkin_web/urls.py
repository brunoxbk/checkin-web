from django.conf.urls import url, include
from django.conf.urls.static import static
from django.urls import path
from django.conf import settings

urlpatterns = [
    path('', include('apps.core.urls')),
    path('event/', include('apps.event.urls')),
    path('user/', include('apps.people.urls')),
    # path('api/', include('apps.api.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    # path('api/', include('apps.api.urls')),
]


if settings.DEBUG:
    urlpatterns += static(
        settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
