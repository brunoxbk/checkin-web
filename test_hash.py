from Crypto.Cipher import XOR
import base64


def encrypt(key, plaintext):
    cipher = XOR.new(key)
    return base64.b64encode(cipher.encrypt(plaintext))


def decrypt(key, ciphertext):
    cipher = XOR.new(key)
    return cipher.decrypt(base64.b64decode(ciphertext))


psw = 'notsosecretkey'
enc = encrypt(psw, '120241:101345')

print(enc)
print(decrypt(psw, enc))
