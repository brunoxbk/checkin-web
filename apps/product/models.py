from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse


class Product(models.Model):
    event = models.ForeignKey(
        'event.Event', blank=True, null=True, verbose_name=_("Evento"),
        on_delete=models.DO_NOTHING, related_name='product_event')
    name = models.CharField(
        "Nome", max_length=255, null=False, blank=False)
    price = models.DecimalField(
        "Preço", max_digits=8, decimal_places=2, default=0)
    created_at = models.DateTimeField("Created at", auto_now_add=True)
    updated_at = models.DateTimeField("Updated at", auto_now=True)
    status = models.BooleanField("Status", default=True)

    def get_absolute_url(self):
        return reverse(
            'event:product:detail',
            kwargs={'event_id': self.event.id, 'pk': self.id})

    class Meta:
        verbose_name = "Produto"
        verbose_name_plural = "Produtos"
        get_latest_by = 'created_at'
        ordering = ('-created_at',)


class Sale(models.Model):
    seller = models.ForeignKey(
        'people.User', blank=False, null=False, on_delete=models.CASCADE,
        verbose_name="Usuário", related_name='user_seller')
    buyer = models.ForeignKey(
        'people.User', blank=False, null=False, on_delete=models.CASCADE,
        verbose_name="Usuário", related_name='user_buyer')
    product = models.ForeignKey(
        'product.Product', blank=False, null=False, on_delete=models.CASCADE,
        verbose_name="Produto", related_name='product_sale')

    created_at = models.DateTimeField("Created at", auto_now_add=True)
    updated_at = models.DateTimeField("Updated at", auto_now=True)
    status = models.BooleanField("Status", default=True)

    class Meta:
        verbose_name = "Venda"
        verbose_name_plural = "Vendas"
        get_latest_by = 'created_at'
        ordering = ('-created_at',)


@receiver(post_save, sender=Sale, dispatch_uid='sale_callback')
def sale_callback(sender, instance, *args, **kwargs):
    user = instance.user
    user.balance -= instance.product.price
    user.save()
