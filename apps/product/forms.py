from django import forms
from apps.event.models import Event
from apps.product.models import Product


class ProductForm(forms.ModelForm):
    event = forms.ModelChoiceField(
        label='', queryset=Event.objects.all(), widget=forms.HiddenInput)

    class Meta:
        model = Product
        fields = ['name', 'price', 'status', 'event']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'price': forms.NumberInput(attrs={'class': 'form-control'}),
            'event': forms.HiddenInput
        }
        fieldsets = (
            ('', {'fields': ('name',)}),
            ('', {'fields': ('status',)}),
            ('', {'fields': ('price',)}),
            ('', {'fields': ('event',)}),
        )
