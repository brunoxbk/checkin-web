# Create your views here.
from django.views.generic import ListView, CreateView, \
    DetailView, UpdateView, DeleteView
from apps.product.models import Product
from apps.event.models import Event
from apps.product.forms import ProductForm
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404


class ProductListView(ListView):
    model = Product

    def get_queryset(self, **kwargs):
        return self.model.objects.filter(event_id=self.kwargs.get('event_id'))

    def get_context_data(self, **kwargs):
        context = super(ProductListView, self).get_context_data(**kwargs)

        event = get_object_or_404(Event, id=self.kwargs.get('event_id'))

        context.update(dict(event=event))
        return context


class ProductCreateView(CreateView):
    model = Product
    form_class = ProductForm

    def get_initial(self):
        self.initial = {'event': self.kwargs.get('event_id')}
        return self.initial.copy()


class ProductDetailView(DetailView):
    model = Product


class ProductUpdateView(UpdateView):
    model = Product
    form_class = ProductForm


class ProductDeleteView(DeleteView):
    model = Product
    success_url = '/'
