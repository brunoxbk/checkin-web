# Generated by Django 2.1 on 2018-08-22 02:23

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='user',
            old_name='brith',
            new_name='birth',
        ),
    ]
