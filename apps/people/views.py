# Create your views here.
from django.views.generic import ListView, CreateView, \
    DetailView, UpdateView, DeleteView
from django.views.generic import View
from apps.people.models import User, Picture
from django.shortcuts import get_object_or_404
from apps.people.forms import UserForm2, PictureForm, PictureSaveForm
from django.urls import reverse
from django.conf import settings
from django.http import JsonResponse
from PIL import Image
import base64
import os
import uuid
import re
import io


class UserMixin(object):
    # @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(UserMixin, self)\
            .dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(UserMixin, self).get_context_data(**kwargs)
        # context.update(dict(event=event))
        return context


class UserListView(ListView):
    model = User


class UserCreateView(CreateView):
    model = User
    form_class = UserForm2


class UserDetailView(DetailView):
    model = User

    def get_context_data(self, **kwargs):
        context = super(UserDetailView, self).get_context_data(**kwargs)
        form = PictureForm(initial={'user': self.object.id})
        context.update(dict(form=form))
        return context


class UserUpdateView(UpdateView):
    model = User
    form_class = UserForm2


class UserDeleteView(DeleteView):
    model = User
    success_url = '/'


class PictureCreateView(CreateView):
    model = Picture
    form_class = PictureForm


class PictureDeleteView(DeleteView):
    model = Picture
    pk_url_kwarg = 'picture_id'

    def get_success_url(self):
        return reverse('people:detail', kwargs={'pk': self.kwargs.get('pk')})

    def get_context_data(self, **kwargs):
        context = super(PictureDeleteView, self).get_context_data(**kwargs)
        user = get_object_or_404(User, id=self.kwargs.get('pk'))
        context.update(dict(user=user))
        return context


class PictureDetailView(DetailView):
    model = Picture
    pk_url_kwarg = 'picture_id'


class PictureView(View):

    def post(self, request, *args, **kwargs):
        response = {'saved': False}
        form = PictureSaveForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            data = form.cleaned_data
            print(data)
            image_data = re.sub('^data:image/.+;base64,', '', data['image'])
            im = Image.open(io.BytesIO(base64.b64decode(image_data)))

            temp_filename = '%s.png' % str(uuid.uuid1())
            temp_pach = os.path.join(settings.MEDIA_ROOT, "uploads/user")
            temp_pach_name = os.path.join(temp_pach, temp_filename)

            if not os.path.exists(temp_pach):
                os.makedirs(temp_pach)

            im.save(temp_pach_name)

            url = temp_pach_name.replace(settings.MEDIA_ROOT, '')

            pic = Picture()

            url = url[1:len(url)]

            pic.photo = url

            pic.user = data['user']

            pic.save()

            response = {'saved': True}

        return JsonResponse(response)
