from rest_framework import serializers
from django.shortcuts import get_object_or_404
from apps.sales.models import Product, Sale
from apps.users.models import User
from django.utils import timezone


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'name', 'status', 'price')


class UserSerializer(serializers.ModelSerializer):
    photo = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'email', 'balance', 'photo')

    def get_photo(self, obj):
        if obj.photo:
            return obj.photo.url.split('f2f')[1]
        else:
            return None

    def create(self, validated_data):
        """
            Create and return a new `Snippet` instance,
            given the validated data.
        """
        user = User(
            username=validated_data['email'],
            email=validated_data['email'],
            balance=validated_data['balance']
        )
        user.save()
        return user


class SaleSerializer(serializers.ModelSerializer):
    user_data = serializers.SerializerMethodField()
    product_data = serializers.SerializerMethodField()
    user = serializers.CharField()

    class Meta:
        model = Sale
        fields = ('id', 'user', 'user_data', 'product', 'product_data')

    def get_user(self, obj):
        return obj.email

    def get_user_data(self, obj):
        return UserSerializer(obj.user).data

    def get_product_data(self, obj):
        return ProductSerializer(obj.product).data

    def create(self, validated_data):
        sale = Sale(
            user=get_object_or_404(User, email=validated_data['user']),
            product=validated_data['product']
        )
        sale.save()
        return sale
