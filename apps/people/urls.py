from django.urls import path
from apps.people import views

app_name = 'people'

urlpatterns = [
    path('', views.UserListView.as_view(), name='list'),
    path('add/', views.UserCreateView.as_view(), name='add'),
    path('detail/<int:pk>/', views.UserDetailView.as_view(), name='detail'),
    path('edit/<int:pk>/', views.UserUpdateView.as_view(), name='edit'),
    path('delete/<int:pk>/', views.UserDeleteView.as_view(), name='delete'),
    path('picture/', views.PictureCreateView.as_view(), name='picture'),
    path(
        'detail/<int:pk>/picture-delete/<int:picture_id>/',
        views.PictureDeleteView.as_view(),
        name='picture-delete'),
    path(
        'detail/<int:pk>/picture-detail/<int:picture_id>/',
        views.PictureDetailView.as_view(),
        name='picture-detail'),
    path(
        'picture-save/',
        views.PictureView.as_view(),
        name='picture-save'),
]
