from django import forms
from django.conf import settings
from apps.people.models import User, Picture
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib.auth.models import Group
import re
from django.core.exceptions import ValidationError


class UserForm2(forms.ModelForm):
    class Meta:
        model = User
        fields = [
            'email', 'first_name', 'last_name', 'phone', 'birth', 'rg',
            'cpf', 'gender', 'scholarity', 'city', 'province', 'street',
            'number', 'complement', 'neighborhood', 'postal_code']
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
            'phone': forms.TextInput(attrs={'class': 'form-control'}),
            'cpf': forms.TextInput(attrs={'class': 'form-control'}),
            'rg': forms.TextInput(attrs={'class': 'form-control'}),
            'birth': forms.DateInput(
                        format="%d/%m/%Y", attrs={'class': 'form-control'}),
            'gender': forms.Select(attrs={'class': 'form-control'}),

            'scholarity': forms.Select(attrs={'class': 'form-control'}),

            'province': forms.Select(attrs={'class': 'form-control'}),
            'city': forms.TextInput(attrs={'class': 'form-control'}),
            'neighborhood': forms.TextInput(attrs={'class': 'form-control'}),
            'street': forms.TextInput(attrs={'class': 'form-control'}),
            'number': forms.TextInput(attrs={'class': 'form-control'}),
            'complement': forms.Textarea(attrs={'class': 'form-control'}),
            'postal_code': forms.TextInput(attrs={'class': 'form-control'})
        }
        fieldsets = (
            ('Dados Pessoais', {
                'fields': (
                    'first_name', 'last_name',
                    'gender', 'birth', 'rg', 'cpf',)
            }),
            ('Contato', {
                'fields': ('phone', 'email',)
            }),
            ('Localização', {
                'fields': (
                    'province', 'city', 'neighborhood', 'street',
                    'number', 'postal_code', 'complement')
            }),
        )


class UserForm3(forms.ModelForm):

    def clean_email(self):
        data = self.cleaned_data['email']

        try:
            user = User.objects.get(email=data)
        except Exception as e:
            print(e)
            user = None

        if not user:
            return data
        else:
            raise ValidationError("Email já cadastrado!")

    class Meta:
        model = User
        fields = [
            'email', 'first_name', 'last_name', 'phone', 'birth', 'gender']
        widgets = {
            'first_name': forms.TextInput(
                attrs={'class': 'form-control', 'required': 'true'}),
            'last_name': forms.TextInput(
                attrs={'class': 'form-control', 'required': 'true'}),
            'email': forms.EmailInput(
                attrs={'class': 'form-control', 'required': 'true'}),
            'phone': forms.TextInput(
                attrs={'class': 'form-control'}),
            'birth': forms.DateInput(
                        format="%d/%m/%Y", attrs={'class': 'form-control'}),
            'gender': forms.Select(attrs={'class': 'form-control'}),
        }
        fieldsets = (
            ('Dados Pessoais', {
                'fields': (
                    'first_name', 'last_name',
                    'gender', 'birth')
            }),
            ('Contato', {
                'fields': ('phone', 'email',)
            }),
        )



class UserForm(forms.Form):
    email = forms.EmailField(
        label='Email', max_length=80, required=True,
        widget=forms.EmailInput(
            attrs={
                'required': True,
                'autocomplete': 'off',
                'placeholder': 'Email'}))
    first_name = forms.CharField(
        label='Nome', max_length=120, required=True,
        widget=forms.TextInput(
            attrs={'required': True, 'placeholder': 'Nome'}))
    last_name = forms.CharField(
        label='Sobrenome', max_length=120, required=True,
        widget=forms.TextInput(
            attrs={'required': True, 'placeholder': 'Sobrenome'}))
    phone = forms.CharField(
        label='Telefone', max_length=80, required=True,
        widget=forms.TextInput(
            attrs={'required': True, 'placeholder': 'Telefone'}))

    brith = forms.DateField(
        label='Nascimento', required=True,
        input_formats=settings.DATE_INPUT_FORMATS,
        widget=forms.DateInput(
            attrs={'required': True, 'placeholder': 'Nascimento'}))
    rg = forms.CharField(
        label='RG', required=True,
        widget=forms.TextInput(
            attrs={
                'placeholder': 'RG',
                'autocomplete': 'off',
                'required': True}))
    cpf = forms.CharField(
        label='CPF', required=True,
        widget=forms.TextInput(
            attrs={
                'required': True,
                'autocomplete': 'off',
                'placeholder': 'CPF'}))

    gender = forms.ChoiceField(
        label='Sexo', choices=User.GENDER_CHOICE, required=False,
        widget=forms.Select(attrs={'required': True, }))

    scholarity = forms.ChoiceField(
        label='Escolaridade', choices=User.SCHOLARITY_CHOICE, required=False,
        widget=forms.Select(attrs={'required': True, }))

    city = forms.CharField(
        label='Cidade', required=True,
        widget=forms.TextInput(
            attrs={'required': True, 'placeholder': 'Cidade'}))
    province = forms.ChoiceField(
        label='Estado', choices=User.PROVINCES_CHOICE,
        required=True,
        widget=forms.Select(attrs={}))
    street = forms.CharField(
        label='Rua', required=True,
        widget=forms.TextInput(
            attrs={'required': True, 'placeholder': 'Rua'}))
    number = forms.CharField(
        label='Número', required=True,
        widget=forms.TextInput(
            attrs={'required': True, 'placeholder': 'Número'}))
    complement = forms.CharField(
        label='Complemento', required=False,
        widget=forms.TextInput(
            attrs={'required': False, 'placeholder': 'Complemento'}))
    district = forms.CharField(
        label='Bairro', required=True,
        widget=forms.TextInput(
            attrs={'required': True, 'placeholder': 'Bairro'}))
    postal_code = forms.CharField(
        label='CEP', required=True,
        widget=forms.TextInput(
            attrs={'required': True, 'placeholder': 'CEP'}))

    def clean_phone(self):
        data = self.cleaned_data['phone']
        return re.sub('[^A-Za-z0-9]+', '', data)

    def clean_password_confirm(self):
        data = self.cleaned_data

        if data["password"] != data['password_confirm']:
            raise ValidationError("Senha não confere!")

        return data['password_confirm']

    def clean_cpf(self):
        data = self.cleaned_data['cpf']
        cpf = re.sub('[^A-Za-z0-9]+', '', data)

        try:
            user = User.objects.get(cpf=cpf)
        except Exception as e:
            user = None

        if not user:
            return cpf
        else:
            raise ValidationError("Cpf já cadastrado!")

    def clean_email(self):
        data = self.cleaned_data['email']

        try:
            user = User.objects.get(email=data)
        except Exception as e:
            user = None

        if not user:
            return data
        else:
            raise ValidationError("Email já cadastrado!")

    class Meta:
        fieldsets = (
            ('Dados Pessoais', {
                'fields': (
                    'first_name', 'last_name',
                    'gender', 'brith', 'rg', 'cpf',)
            }),
            ('Localização', {
                'fields': (
                    'province', 'city', 'street', 'number',
                    'complement', 'district', 'postal_code')
            }),
            ('Contato', {
                'fields': ('phone', 'email',)
            }),
            #     ('Dados Acesso', {
            #         'fields': ('password', 'password_confirm')
            #     }),
        )


class PictureForm(forms.ModelForm):
    user = forms.ModelChoiceField(
        label='', queryset=User.objects.all(),
        widget=forms.HiddenInput)

    class Meta:
        model = Picture
        fields = ['photo', 'user']
        widgets = {
            'user': forms.HiddenInput,
            'photo': forms.FileInput(attrs={
                'required': True,
                'class': 'form-control'}),
        }


class PictureSaveForm(forms.Form):
    user = forms.ModelChoiceField(
        label='User', queryset=User.objects.all(),
        widget=forms.HiddenInput)
    image = forms.CharField(
        label='Image', required=False,
        widget=forms.Textarea(attrs={}))

    class Meta:
        model = Picture
        fields = ['photo', 'user']
        widgets = {
            'user': forms.HiddenInput,
            'photo': forms.FileInput(attrs={
                'required': True,
                'class': 'form-control'}),
        }


class FormProfile(forms.Form):
    first_name = forms.CharField(
        label='Nome', max_length=120, required=True,
        widget=forms.TextInput(
            attrs={
                'required': True,
                'class': 'form-control', 'placeholder': 'Nome'}))
    last_name = forms.CharField(
        label='Sobrenome', max_length=120, required=True,
        widget=forms.TextInput(
            attrs={'required': True, 'placeholder': 'Sobrenome'}))
    phone = forms.CharField(
        label='Telefone', max_length=80, required=True,
        widget=forms.TextInput(
            attrs={
                'required': True,
                'class': 'form-control',
                'placeholder': 'Telefone'}))
    email = forms.EmailField(
        label='Email', max_length=80, required=True,
        widget=forms.EmailInput(
            attrs={
                'required': 'true',
                'class': 'form-control',
                'placeholder': 'Email'}))
    brith = forms.DateField(
        label='Nascimento', required=True,
        input_formats=settings.DATE_INPUT_FORMATS,
        widget=forms.DateInput(
            attrs={
                'required': True,
                'class': 'form-control',
                'placeholder': 'Nascimento'
            }))
    rg = forms.CharField(
        label='RG', required=True,
        widget=forms.TextInput(
            attrs={
                'required': True,
                'class': 'form-control', 'placeholder': 'RG'}))
    cpf = forms.CharField(
        label='CPF', required=True,
        widget=forms.TextInput(
            attrs={
                'required': True,
                'class': 'form-control',
                'placeholder': 'CPF',
                'autocomplete': 'off',
                'readonly': ''}))

    gender = forms.ChoiceField(
        label='Sexo', choices=User.GENDER_CHOICE, required=True,
        widget=forms.Select(attrs={'class': 'form-control'}))

    city = forms.CharField(
        label='Cidade', required=True,
        widget=forms.TextInput(
            attrs={
                'required': 'true',
                'placeholder': 'Cidade'}))

    province = forms.ChoiceField(
        label='Estado', choices=User.PROVINCES_CHOICE,
        required=True,
        widget=forms.Select(attrs={}))

    street = forms.CharField(
        label='Rua', required=True,
        widget=forms.TextInput(
            attrs={'required': True, 'placeholder': 'Rua'}))
    number = forms.CharField(
        label='Número', required=True,
        widget=forms.TextInput(
            attrs={'required': True, 'placeholder': 'Número'}))
    complement = forms.CharField(
        label='Complemento', required=False,
        widget=forms.TextInput(
            attrs={'placeholder': 'Complemento'}))
    district = forms.CharField(
        label='Bairro', required=True,
        widget=forms.TextInput(
            attrs={'required': True, 'placeholder': 'Bairro'}))
    postal_code = forms.CharField(
        label='CEP', required=True,
        widget=forms.TextInput(
            attrs={'required': True, 'placeholder': 'CEP'}))

    password = forms.CharField(
        label='Senha', max_length=80, required=True,
        help_text='Digite sua senha para prosseguir com as alterações',
        widget=forms.PasswordInput(
            attrs={
                'required': True,
                'placeholder': 'Senha',
                'autocomplete': 'off'}))

    id = forms.CharField(
        label='', required=True, widget=forms.HiddenInput)

    def clean_cpf(self):
        data = self.cleaned_data['cpf']
        return re.sub('[^A-Za-z0-9]+', '', data)

    def clean_phone(self):
        data = self.cleaned_data['phone']
        return re.sub('[^A-Za-z0-9]+', '', data)

    def clean(self):
        data = self.cleaned_data
        user = User.objects.get(cpf=data['cpf'])
        if 'password' in data:
            if not user.check_password(data['password']):
                raise ValidationError({'password': ["Senha não confere", ]})
        else:
            raise ValidationError({'password': ["Senha não confere", ]})

        users = User.objects.filter(cpf=data['cpf']).exclude(id=data['id'])

        if users:
            raise ValidationError({'cpf': ["Cpf já cadastrado!", ]})

        users = User.objects.filter(email=data['email']).exclude(id=data['id'])

        if users:
            raise ValidationError({'email': ["Email já em utilização", ]})

    class Meta:
        fieldsets = (
            ('Dados Pessoais', {
                'fields': (
                    'first_name', 'last_name', 'gender', 'brith', 'rg', 'cpf',)
            }),
            ('Localização', {
                'fields': (
                    'province', 'city', 'street', 'number',
                    'complement', 'district', 'postal_code')
            }),
            ('Contato', {
                'fields': ('phone', 'email',)
            }),
            ('Senha', {
                'fields': ('id', 'password', )
            }),
        )


class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Senha', widget=forms.PasswordInput)
    password2 = forms.CharField(
        label='Confirmação de Senha', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name')

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Senhas não conferem")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()
    groups = forms.ModelMultipleChoiceField(
        queryset=Group.objects.all().order_by('name'),
        widget=forms.CheckboxSelectMultiple(), required=False, label='Grupos')

    def __init__(self, *args, **kargs):
        super(UserChangeForm, self).__init__(*args, **kargs)
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            # self.fields['created'].widget.attrs['readonly'] = True
            # self.fields['last_login'].widget.attrs['readonly'] = True
            self.fields['password'].help_text = (
                "Para alterar a senha do usuario entre "
                "<a href=\'/admin/users/user/%d/password/'>"
                "nesse formulário</a>." % instance.pk)

    class Meta:
        model = User
        fields = (
            'username', 'email', 'password',
            'first_name', 'last_name')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]
