from django import forms
from apps.check_in.models import CheckIn


class CheckInForm(forms.ModelForm):

    class Meta:
        model = CheckIn
        fields = ['participant', 'action', 'status']
        widgets = {
            'user': forms.Select(
                attrs={'class': 'form-control', 'readonly': 'true'}),
            'action': forms.Select(
                attrs={'class': 'form-control', 'readonly': 'true'}),
        }
        fieldsets = (
            ('', {'fields': ('user',)}),
            ('', {'fields': ('action',)}),
            ('', {'fields': ('status',)}),
        )


class PictureForm(forms.Form):
    image = forms.CharField(
        label='Image', required=True,
        widget=forms.Textarea(attrs={}))
