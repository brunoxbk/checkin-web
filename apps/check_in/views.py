# Create your views here.
from django.views.generic import ListView, CreateView, \
    DetailView, UpdateView, DeleteView
from apps.check_in.models import CheckIn
from apps.check_in.forms import CheckInForm, PictureForm
from django.http import JsonResponse
from apps.action.models import Action
from django.views.generic import View
from apps.people.models import User
from apps.event.models import Event, Participant
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from django.shortcuts import render
from django.conf import settings
from datetime import datetime
# import face_recognition
# from PIL import Image
import base64
import os
import uuid
import re
import io


# def allowed_file(filename):
#     return '.' in filename and \
#            filename.rsplit('.', 1)[1].lower() in settings.ALLOWED_EXTENSIONS
#
#
# def detect_faces_in_image(file_stream):
#     # Pre-calculated face encoding of
#     # Obama generated with face_recognition.face_encodings(img)
#
#     # Load the uploaded image file
#     img = face_recognition.load_image_file(file_stream)
#     # Get face encodings for any faces in the uploaded image
#     unknown_face_encodings = face_recognition.face_encodings(img)
#
#     face_found = False
#     detected = False
#     detected_user = None
#
#     if len(unknown_face_encodings) > 0:
#         face_found = True
#         for user in User.objects.all():
#             for picture in user.picture_user.all():
#                 match_results = face_recognition.compare_faces(
#                     [picture.numbers], unknown_face_encodings[0])
#                 if match_results[0]:
#                     detected = True
#                     detected_user = user
#                     break
#
#     result = {
#         "face_found_in_image": face_found,
#         "detected": detected,
#         "detected_user": detected_user
#     }
#     return result


class CheckInMixin(object):
    # @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CheckInMixin, self)\
            .dispatch(request, *args, **kwargs)

    def get_initial(self):
        self.initial = {'event': self.kwargs.get('event_id')}
        return self.initial.copy()

    def get_context_data(self, **kwargs):
        context = super(CheckInMixin, self).get_context_data(**kwargs)

        event = get_object_or_404(Event, id=self.kwargs.get('event_id'))
        action = get_object_or_404(Action, id=self.kwargs.get('action_id'))

        context.update(dict(event=event, action=action))
        return context


class CheckInListView(ListView):
    model = CheckIn

    def get_queryset(self, **kwargs):
        return self.model.objects.filter(event_id=self.kwargs.get('event_id'))

    def get_context_data(self, **kwargs):
        context = super(CheckInListView, self).get_context_data(**kwargs)

        event = get_object_or_404(Event, id=self.kwargs.get('event_id'))
        action = get_object_or_404(Action, id=self.kwargs.get('action_id'))

        context.update(dict(event=event, action=action))
        return context


class CheckInCreateView(CreateView):
    model = CheckIn
    form_class = CheckInForm


class CheckInDetailView(DetailView):
    model = CheckIn


class CheckInUpdateView(UpdateView):
    model = CheckIn
    form_class = CheckInForm


class CheckInDeleteView(DeleteView):
    model = Action
    success_url = '/'


# class CheckinActionView(View):
#     template_name = 'check_in/check_in_face.html'
#
#     def get(self, request, *args, **kwargs):
#         form = CheckInForm()
#         event = get_object_or_404(Event, id=self.kwargs.get('event_id'))
#         action = get_object_or_404(Action, id=self.kwargs.get('action_id'))
#         return render(request, self.template_name, locals())
#
#     @csrf_exempt
#     def post(self, request, *args, **kwargs):
#         response = {
#             'face_found_in_image': False,
#             'detected': False,
#             'detected_user': None,
#             'checkin': False,
#             'exists': False,
#         }
#
#         event = get_object_or_404(Event, id=self.kwargs.get('event_id'))
#         action = get_object_or_404(Action, id=self.kwargs.get('action_id'))
#
#         form = PictureForm(request.POST)
#         # check whether it's valid:
#         if form.is_valid():
#             data = form.cleaned_data
#
#             event = get_object_or_404(Event, id=self.kwargs.get('event_id'))
#             action = get_object_or_404(Action, id=self.kwargs.get('action_id'))
#
#             image_data = re.sub('^data:image/.+;base64,', '', data['image'])
#             im = Image.open(io.BytesIO(base64.b64decode(image_data)))
#             temp_filename = '%s.png' % str(uuid.uuid1())
#             temp_pach = os.path.join(settings.MEDIA_ROOT, "tmp")
#             temp_pach_name = os.path.join(temp_pach, temp_filename)
#
#             if not os.path.exists(temp_pach):
#                 os.makedirs(temp_pach)
#
#             im.save(temp_pach_name)
#
#             response = detect_faces_in_image(temp_pach_name)
#
#             if response['detected'] and response['detected_user']:
#                 response['exists'] = True
#
#                 p, pcreated = Participant.objects.get_or_create(
#                     user=response['detected_user'],
#                     event=event,
#                     defaults={}
#                 )
#
#                 c, ccreated = CheckIn.objects.get_or_create(
#                     participant=p,
#                     action=action,
#                     defaults={}
#                 )
#
#                 if ccreated:
#                     response['checkin'] = True
#                 else:
#                     response['exists'] = True
#
#                 response['detected_user'] = response['detected_user'].to_json()
#             print(response)
#             return JsonResponse(response)
