from django.db import models
from django.utils.translation import ugettext_lazy as _


class CheckIn(models.Model):
    """Model Check in."""

    participant = models.ForeignKey(
        'event.Participant', blank=True, null=True,
        verbose_name=_("Participante"),
        on_delete=models.DO_NOTHING,
        related_name='check_in_participant')
    action = models.ForeignKey(
        'action.Action', blank=True, null=True, verbose_name=_("Ação"),
        on_delete=models.DO_NOTHING, related_name='check_in_action')
    status = models.BooleanField(_('Status'), default=False)
    created = models.DateTimeField(_('Criado'), auto_now_add=True)
    changed = models.DateTimeField(_('Alterado'), auto_now=True)

    def __str__(self):
        """Representation to str."""
        return '<CheckIn %d>' % self.id

    class Meta:
        verbose_name = "Check in"
        verbose_name_plural = "Check ins"
        get_latest_by = 'created'
        ordering = ('-created',)
