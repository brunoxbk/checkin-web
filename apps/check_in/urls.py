from django.urls import path
from apps.check_in import views

app_name = 'check_in'

urlpatterns = [
    path('', views.CheckInListView.as_view(), name='list'),
    # path('add/', views.CheckInCreateView.as_view(), name='add'),
    # path('detect/', views.CheckinActionView.as_view(), name='detect'),
    path('detail/<int:pk>/', views.CheckInDetailView.as_view(), name='detail'),
    path('edit/<int:pk>/', views.CheckInUpdateView.as_view(), name='edit'),
    path('delete/<int:pk>/', views.CheckInDeleteView.as_view(), name='delete'),
]
