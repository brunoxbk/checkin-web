from django.urls import path, include
from apps.participant import views

app_name = 'participant'

urlpatterns = [
    path('', views.ParticipantListView.as_view(), name='list'),
    path('add/', views.ParticipantCreateView.as_view(), name='add'),
    path(
        'detail/<int:pk>/',
        views.ParticipantDetailView.as_view(), name='detail'),
    path(
        'edit/<int:pk>/',
        views.ParticipantUpdateView.as_view(), name='edit'),
    path(
        'delete/<int:pk>/',
        views.ParticipantDeleteView.as_view(), name='delete'),
    path('balance/', include('apps.balance.urls')),
]
