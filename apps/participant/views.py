# Create your views here.
from django.views.generic import ListView, CreateView, \
    DetailView, UpdateView, DeleteView
from apps.event.models import Participant
from apps.event.models import Event
from apps.participant.forms import ParticipantForm
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404


class ParticipantMixin(object):
    # @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ParticipantMixin, self)\
            .dispatch(request, *args, **kwargs)

    def get_initial(self):
        self.initial = {'event': self.kwargs.get('event_id')}
        return self.initial.copy()

    def get_context_data(self, **kwargs):
        context = super(ParticipantMixin, self).get_context_data(**kwargs)

        event = get_object_or_404(Event, id=self.kwargs.get('event_id'))

        context.update(dict(event=event))
        return context


class ParticipantListView(ParticipantMixin, ListView):
    model = Participant


class ParticipantCreateView(ParticipantMixin, CreateView):
    model = Participant
    form_class = ParticipantForm


class ParticipantDetailView(DetailView):
    model = Participant


class ParticipantUpdateView(UpdateView):
    model = Participant
    form_class = ParticipantForm


class ParticipantDeleteView(DeleteView):
    model = Participant
    success_url = '/'
