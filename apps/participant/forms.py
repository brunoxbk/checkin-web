from django import forms
from django.conf import settings
from apps.people.models import User
from apps.event.models import Participant, Event


class ParticipantForm(forms.ModelForm):
    # event = forms.CharField(label='', widget=forms.HiddenInput)
    user = forms.ModelChoiceField(
        label='', queryset=User.objects.all(),
        widget=forms.Select(attrs={'class': 'form-control'}))
    event = forms.ModelChoiceField(
        label='', queryset=Event.objects.all(),
        widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = Participant
        fields = ['user', 'event', 'status']
        fieldsets = (
            ('', {'fields': ('event',)}),
            ('', {'fields': ('user',)}),
            ('', {'fields': ('status',)}),
        )
