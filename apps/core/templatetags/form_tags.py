from django import template

register = template.Library()


@register.simple_tag()
def get_label(field, form):
    if field != '__all__':
        label = form.fields[field].label
    else:
        label = 'Erro'
    return label
