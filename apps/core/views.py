from django.views.generic.base import TemplateView
from django.views.generic import CreateView
from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.utils.decorators import method_decorator
from django.views.generic import FormView, RedirectView
from django.contrib.auth.decorators import login_required
from apps.core.forms import FormCustomLogin, SignUpForm, FormCertificate
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect
from django.contrib import messages
from apps.event.models import Event
from apps.people.models import User
from apps.people.forms import UserForm3

import io
from xhtml2pdf import pisa
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse
from cgi import escape
from weasyprint import HTML


def render_to_pdf(template_src, context_dict):
    htmlstring = get_template(template_src).render(context_dict)
    html = HTML(string=htmlstring)
    main_doc = html.render()
    pdf = main_doc.write_pdf()
    return HttpResponse(pdf, content_type='application/pdf')


class CoreMixin(object):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CoreMixin, self)\
            .dispatch(request, *args, **kwargs)


class IndexView(TemplateView):
    template_name = "core/index.html"

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context.update(dict(events=Event.objects.all()))
        return context


class HomeView(TemplateView):
    template_name = "core/index.html"


class LoginView(FormView):
    success_url = '/'
    form_class = FormCustomLogin
    # redirect_field_name = REDIRECT_FIELD_NAME
    template_name = "core/login.html"

    def form_valid(self, form):
        data = form.cleaned_data

        user = authenticate(
            self.request,
            username=data['username'],
            password=data['password'])

        if user is not None:
            if user.is_active:
                login(self.request, user)
                return HttpResponseRedirect('/')
            else:
                msg = 'Conta inativa'
                messages.add_message(self.request, messages.WARNING, msg)
                return HttpResponseRedirect('/login/')
        else:
            msg = 'Usuário não encontrado'
            messages.add_message(self.request, messages.WARNING, msg)
            return HttpResponseRedirect('/login/')
        return super(LoginView, self).form_valid(form)


class FormCertificate(FormView):
    success_url = '/'
    form_class = FormCertificate
    # redirect_field_name = REDIRECT_FIELD_NAME
    template_name = "core/sing_up.html"

    def form_valid(self, form):
        data = form.cleaned_data

        try:
            user = User.objects.get(email=data['email'])
        except Exception as e:
            user = None

        if user is None:
            msg = "Ops, tivemos um probleminha com seu cadastro," \
                "por favor faça um novo cadastro para receber seu certificado."
            messages.add_message(self.request, messages.WARNING, msg)
            return HttpResponseRedirect('/')
        else:
            context = {'user': user}
            # return render(self.request, 'core/certificate.html', context)
            return render_to_pdf('core/certificate.html', context)

        return super(FormCertificate, self).form_valid(form)


class LogoutView(CoreMixin, RedirectView):
    url = '/login/'
    permanent = True

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)


class SingUpView(FormView):
    success_url = '/'
    form_class = SignUpForm
    # redirect_field_name = REDIRECT_FIELD_NAME
    template_name = "core/sing_up.html"

    def form_valid(self, form):

        data = form.cleaned_data
        user = User(**data)

        user.set_password(data['password'])

        user.save()

        msg = 'Conta inativa'
        messages.add_message(self.request, messages.WARNING, msg)
        return HttpResponseRedirect('/login/')

        return super(SingUpView, self).form_valid(form)


class UserCreateView(SuccessMessageMixin, CreateView):
    model = User
    form_class = UserForm3
    success_url = '/'
    success_message = "Cadastro realizado com sucesso."

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        data = form.cleaned_data
        user = User(**data)

        user.set_password("admin@12345")
        self.object = user
        return super().form_valid(form)
