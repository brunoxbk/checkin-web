from django import forms
from apps.people.models import User


class FormCustomLogin(forms.Form):
    username = forms.CharField(
        label='Username', max_length=80, required=True,
        widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(
        label='Senha', max_length=80, required=True,
        widget=forms.PasswordInput(attrs={'class': 'form-control'}))


class FormCertificate(forms.Form):
    email = forms.EmailField(
        label='Email', max_length=80, required=True,
        widget=forms.EmailInput(attrs={'class': 'form-control'}),)

    class Meta:
        fieldsets = (('', {'fields': ('email', )}),)


class SignUpForm(forms.ModelForm):
    first_name = forms.CharField(
        label='Nome', max_length=80, required=True,
        widget=forms.TextInput(attrs={'autocomplete': 'off'}))
    last_name = forms.CharField(
        label='Sobrenome', max_length=80, required=True,
        widget=forms.TextInput(attrs={'autocomplete': 'off'}))

    class Meta:
        model = User
        fields = [
            'email', 'phone', 'birth', 'rg',
            'cpf', 'gender', 'scholarity', 'city', 'province', 'street',
            'number', 'complement', 'neighborhood', 'postal_code']
        widgets = {
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
            'phone': forms.TextInput(attrs={'class': 'form-control'}),
            'cpf': forms.TextInput(attrs={'class': 'form-control'}),
            'rg': forms.TextInput(attrs={'class': 'form-control'}),
            'birth': forms.DateInput(
                        format="%d/%m/%Y", attrs={'class': 'form-control'}),
            'gender': forms.Select(attrs={'class': 'form-control'}),

            'scholarity': forms.Select(attrs={'class': 'form-control'}),

            'province': forms.Select(attrs={'class': 'form-control'}),
            'city': forms.TextInput(attrs={'class': 'form-control'}),
            'neighborhood': forms.TextInput(attrs={'class': 'form-control'}),
            'street': forms.TextInput(attrs={'class': 'form-control'}),
            'number': forms.TextInput(attrs={'class': 'form-control'}),
            'complement': forms.Textarea(attrs={'class': 'form-control'}),
            'postal_code': forms.TextInput(attrs={'class': 'form-control'})
        }
        fieldsets = (
            ('Dados Pessoais', {
                'fields': (
                    'first_name', 'last_name',
                    'gender', 'birth', 'rg', 'cpf',)
            }),
            ('Contato', {
                'fields': ('phone', 'email',)
            }),
            ('Localização', {
                'fields': (
                    'province', 'city', 'neighborhood', 'street',
                    'number', 'postal_code', 'complement')
            }),
        )
