from django.urls import path
from apps.core import views
from django.contrib.auth.views import LogoutView


app_name = 'core'

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('certificate/', views.FormCertificate.as_view(), name='certificate'),
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('sing_up/', views.UserCreateView.as_view(), name='sing_up'),
]
