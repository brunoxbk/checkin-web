from django.urls import path
from apps.api import views

app_name = 'api'

urlpatterns = [
    path('event/', views.EventList.as_view(), name='event-list'),
    path(
        'action/<int:event_id>/',
        views.ActionList.as_view(),
        name='action-detail'),
    path(
        'participant/<int:event_id>/',
        views.ActionList.as_view(),
        name='participant-list'),
    path(
        'checkin/',
        views.CheckInList.as_view(),
        name='checkin-list'),
    path(
        'product/<int:event_id>/',
        views.ProductList.as_view(),
        name='checkin-list'),
]
