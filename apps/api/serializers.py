from rest_framework import serializers
from django.shortcuts import get_object_or_404
from apps.event.models import Event, Participant
from apps.check_in.models import CheckIn
from apps.action.models import Action
from apps.people.models import User
from apps.product.models import Product
from django.utils import timezone


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = ('id', 'name', 'text', 'period')


class ParticipantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Participant
        fields = ('id', 'user', 'event', 'status')


class ActionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Action
        fields = ('id', 'name', 'text', 'event')


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'name', 'price', 'status', 'event')


class ParticipantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Participant
        fields = ('id', 'user', 'code', 'event', 'status')


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'email', 'first_name', 'last_name')


class CheckInSerializer(serializers.ModelSerializer):
    participant = serializers.CharField()

    class Meta:
        model = CheckIn
        fields = ('id', 'participant', 'action',)
