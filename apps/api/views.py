from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from apps.api.serializers import EventSerializer, \
    UserSerializer, ActionSerializer, ProductSerializer, \
    ParticipantSerializer, CheckInSerializer
from apps.event.models import Event, Participant
from apps.check_in.models import CheckIn
from apps.action.models import Action
from apps.product.models import Product
from apps.people.models import User
from django.shortcuts import get_object_or_404
from rest_framework import generics
from apps.event.models import Event


class EventList(generics.ListCreateAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer


class ActionList(generics.ListCreateAPIView):
    queryset = Action.objects.all()
    serializer_class = ActionSerializer

    def get_queryset(self):
        objects = Action.objects.filter(event_id=self.kwargs['event_id'])
        serializer = ActionSerializer(objects, many=True)
        return objects


class ParticipantList(generics.ListCreateAPIView):
    queryset = Participant.objects.all()
    serializer_class = ParticipantSerializer

    def get_queryset(self):
        objects = Participant.objects.filter(event_id=self.kwargs['event_id'])
        serializer = ParticipantSerializer(objects, many=True)
        return objects


class CheckInList(generics.ListCreateAPIView):
    queryset = CheckIn.objects.all()
    serializer_class = CheckInSerializer

    def post(self, request, format=None):
        serializer = CheckInSerializer(data=request.data)
        if serializer.is_valid():
            # serializer.create()
            ch = CheckIn(
                participant=get_object_or_404(
                    Participant, code=request.data['participant']),
                action=get_object_or_404(
                    Action, id=request.data['action']))
            ch.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProductList(generics.ListCreateAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def get_queryset(self):
        objects = Product.objects.filter(event_id=self.kwargs['event_id'])
        serializer = ProductSerializer(objects, many=True)
        return objects


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
