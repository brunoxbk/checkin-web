# Create your views here.
from django.views.generic import ListView, CreateView, \
    DetailView, UpdateView, DeleteView
from apps.action.models import Action
from apps.event.models import Event
from apps.action.forms import ActionForm
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404


class ActionMixin(object):
    # @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ActionMixin, self)\
            .dispatch(request, *args, **kwargs)

    def get_initial(self):
        self.initial = {'event': self.kwargs.get('event_id')}
        return self.initial.copy()

    def get_context_data(self, **kwargs):
        context = super(ActionMixin, self).get_context_data(**kwargs)

        event = get_object_or_404(Event, id=self.kwargs.get('event_id'))

        context.update(dict(event=event))
        return context


class ActionListView(ActionMixin, ListView):
    model = Action

    def get_queryset(self, **kwargs):
        return self.model.objects.filter(event_id=self.kwargs.get('event_id'))


class ActionCreateView(ActionMixin, CreateView):
    model = Action
    form_class = ActionForm


class ActionDetailView(ActionMixin, DetailView):
    model = Action


class ActionUpdateView(ActionMixin, UpdateView):
    model = Action
    form_class = ActionForm


class ActionDeleteView(ActionMixin, DeleteView):
    model = Action
    success_url = '/'
