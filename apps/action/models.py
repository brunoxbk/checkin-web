from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse


class Action(models.Model):
    event = models.ForeignKey(
        'event.Event', blank=True, null=True, verbose_name=_("Evento"),
        on_delete=models.DO_NOTHING, related_name='action_event')
    name = models.CharField(_('Nome'), max_length=255, blank=False)
    date = models.DateField(_("Date"), blank=True, null=True)
    banner = models.ImageField(
        _('Banner'), upload_to='uploads/action/', null=True, blank=True)
    text = models.TextField("Texto", null=True, blank=True)
    created = models.DateTimeField(_('Criado'), auto_now_add=True)
    changed = models.DateTimeField(_('Alterado'), auto_now=True)

    def get_absolute_url(self):
        return reverse(
            'event:action:detail',
            kwargs={'event_id': self.event.id, 'pk': self.id})

    def __str__(self):
        """Ação to str."""
        return '<Action %s>' % self.name

    class Meta:
        verbose_name = "Ação"
        verbose_name_plural = "Ações"
        get_latest_by = 'created'
        ordering = ('-created',)
