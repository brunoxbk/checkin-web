from django import forms
from django.conf import settings
from apps.event.models import Event
from apps.action.models import Action
import re


class ActionForm(forms.ModelForm):
    # event = forms.CharField(label='', widget=forms.HiddenInput)
    event = forms.ModelChoiceField(
        label='',
        queryset=Event.objects.all(),
        widget=forms.HiddenInput)

    class Meta:
        model = Action
        fields = ['name', 'date', 'banner', 'text', 'event']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'date': forms.DateInput(
                        format="%d/%m/%Y",
                        attrs={'class': 'form-control'}),
            'banner': forms.ClearableFileInput(
                        attrs={'class': 'form-control'}),
            'text': forms.Textarea(attrs={'class': 'form-control'}),
            'event': forms.HiddenInput
        }
        fieldsets = (
            ('', {'fields': ('name',)}),
            ('', {'fields': ('date',)}),
            ('', {'fields': ('banner',)}),
            ('', {'fields': ('text',)}),
            ('', {'fields': ('event',)}),
        )
