from django.urls import path, include
from apps.action import views

app_name = 'action'

urlpatterns = [
    path('', views.ActionListView.as_view(), name='list'),
    path('add/', views.ActionCreateView.as_view(), name='add'),
    path('detail/<int:pk>/', views.ActionDetailView.as_view(), name='detail'),
    path('edit/<int:pk>/', views.ActionUpdateView.as_view(), name='edit'),
    path('delete/<int:pk>/', views.ActionDeleteView.as_view(), name='delete'),
    # path('<int:action_id>/', include('apps.check_in.urls')),
]
