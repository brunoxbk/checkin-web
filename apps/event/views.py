# Create your views here.
from django.views.generic import ListView, CreateView, \
    DetailView, UpdateView, DeleteView
from django.views.generic.edit import FormView
from apps.event.models import Event
from apps.event.forms import EventForm, CodeForm
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.urls import reverse_lazy
from django.conf import settings
from django.contrib import messages
from django.template.defaultfilters import slugify
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from Crypto.Cipher import XOR
import base64
import os


def encrypt(key, plaintext):
    cipher = XOR.new(key)
    return base64.b64encode(cipher.encrypt(plaintext))


def decrypt(key, ciphertext):
    cipher = XOR.new(key)
    return cipher.decrypt(base64.b64decode(ciphertext))


class EventMixin(object):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EventMixin, self)\
            .dispatch(request, *args, **kwargs)


class EventListView(EventMixin, ListView):
    model = Event


class EventCreateView(EventMixin, CreateView):
    model = Event
    form_class = EventForm


class EventDetailView(EventMixin, DetailView):
    model = Event


class EventUpdateView(EventMixin, UpdateView):
    model = Event
    form_class = EventForm


class EventDeleteView(EventMixin, DeleteView):
    model = Event
    success_url = '/event/'


class CodeFormView(EventMixin, FormView):
    template_name = "event/event_form.html"
    form_class = CodeForm
    success_url = '/event/'

    def form_valid(self, form):
        data = form.cleaned_data
        event = get_object_or_404(Event, id=self.kwargs.get('pk'))
        path = os.path.join(settings.MEDIA_ROOT, "uploads", "codes")
        if not os.path.exists(path):
            os.makedirs(path)

        name = slugify("%d %s" % (event.id, event.name))
        filename = os.path.join(path, "%s.txt" % name)

        print(filename)

        the_file = open(filename, 'w')
        the_file.write('')
        the_file.close()

        for i in range(int(data['start']), int(data['end']) + 1):
            with open(filename, 'a') as the_file:
                code = encrypt(event.secret_key, "%d:%d" % (event.id, i))
                the_file.write('\"%s\"\n' % code.decode("utf-8"))
                the_file.close()

        p = filename.replace(settings.MEDIA_ROOT, '')

        print(p[1:len(p)])

        event.start_code = int(data['start'])
        event.end_code = int(data['end'])
        event.codes = p[1:len(p)]
        event.save()

        msg = "Códigos gerados com sucesso."
        messages.add_message(self.request, messages.SUCCESS, msg)
        return super(CodeFormView, self).form_valid(form)
