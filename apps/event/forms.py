from django import forms
from django.conf import settings
from apps.event.models import Event


class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = ['name', 'date', 'secret_key', 'banner', 'text', 'period']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'secret_key': forms.TextInput(attrs={'class': 'form-control'}),
            'date': forms.DateInput(
                        format="%d/%m/%Y",
                        attrs={'class': 'form-control'}),
            'banner': forms.ClearableFileInput(
                        attrs={'class': 'form-control'}),
            'text': forms.Textarea(attrs={'class': 'form-control'}),
            'period': forms.Select(attrs={'class': 'form-control'}),
        }
        fieldsets = (
            ('', {'fields': ('name',)}),
            ('', {'fields': ('date',)}),
            ('', {'fields': ('secret_key',)}),
            ('', {'fields': ('banner',)}),
            ('', {'fields': ('period',)}),
            ('', {'fields': ('text',)}),
        )


class CodeForm(forms.Form):
    start = forms.CharField(
        label='Início', required=True, initial="0",
        widget=forms.NumberInput(attrs={'class': 'form-control'}))
    end = forms.CharField(
        label='Fim', required=True, initial="0",
        widget=forms.NumberInput(attrs={'class': 'form-control'}))

    class Meta:
        fieldsets = (
            ('', {'fields': ('start',)}),
            ('', {'fields': ('end',)}),
        )
