from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.db.models import Sum
from django.urls import reverse


class Event(models.Model):
    PERIOD_CHOICE = (
        ('', '---------'),
        ('M', 'Matutino'),
        ('V', 'Vespetino'),
        ('N', 'Noturno'),
        ('I', 'Integral'),
    )
    name = models.CharField(
        _('Nome'), max_length=255, blank=False)
    date = models.DateField(_("Date"), blank=True, null=True)
    secret_key = models.CharField(
        _('Palavra chave'), max_length=255, blank=False)
    banner = models.ImageField(
        _('Banner'), upload_to='uploads/banner/', null=True, blank=True)
    text = models.TextField("Texto", null=True, blank=True)
    period = models.CharField(
        _('Periodo'), max_length=2, choices=PERIOD_CHOICE)
    created = models.DateTimeField(_('Criado'), auto_now_add=True)
    changed = models.DateTimeField(_('Alterado'), auto_now=True)
    start_code = models.IntegerField(_('Inicio códigos'), default=0)
    end_code = models.IntegerField(_('Fim códigos'), default=0)
    codes = models.FileField(blank=True, upload_to='uploads/codes/')

    participants = models.ManyToManyField(
        'people.User',
        through='event.Participant',
        through_fields=('event', 'user'),
    )

    @property
    def get_total_codes(self):
        return self.end_code - self.start_code

    def get_absolute_url(self):
        # return reverse('people.views.details', args=[str(self.id)])
        return reverse('event:detail', kwargs={'pk': self.id})

    def get_codes(self):
        pass

    def __str__(self):
        """Representation to str."""
        return '<Event %s>' % self.name

    class Meta:
        verbose_name = "Evento"
        verbose_name_plural = "Eventos"
        get_latest_by = 'created'
        ordering = ('-created',)


class Participant(models.Model):
    user = models.ForeignKey(
        'people.User', blank=True, null=True, verbose_name=_("Usuário"),
        on_delete=models.DO_NOTHING, related_name='participant_user')
    event = models.ForeignKey(
        'event.Event', blank=True, null=True, verbose_name=_("Evento"),
        on_delete=models.DO_NOTHING, related_name='participant_event')
    status = models.BooleanField("Status", default=True)
    created_at = models.DateTimeField("Created at", auto_now_add=True)
    updated_at = models.DateTimeField("Updated at", auto_now=True)
    code = models.CharField(
        _('Codigo'), max_length=255, blank=True)

    def get_credit(self):
        credit = self.movement_participant.filter(kind='C')\
            .aggregate(Sum('value'))
        debit = self.movement_participant.filter(kind='D')\
            .aggregate(Sum('value'))
        print(credit, debit)
        if credit['value__sum'] is None:
            credit['value__sum'] = 0

        if debit['value__sum'] is None:
            debit['value__sum'] = 0

        return credit['value__sum'] - debit['value__sum']

    def get_absolute_url(self):
        return reverse(
            'event:participant:detail',
            kwargs={'event_id': self.id, 'pk': self.id})

    class Meta:
        verbose_name = "Participante"
        verbose_name_plural = "Participantes"
        get_latest_by = 'created_at'
        ordering = ('-created_at',)
