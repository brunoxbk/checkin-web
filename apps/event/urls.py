from django.urls import path, include
from apps.event import views

app_name = 'event'

urlpatterns = [
    path('', views.EventListView.as_view(), name='list'),
    path('add/', views.EventCreateView.as_view(), name='add'),
    path('detail/<int:pk>/', views.EventDetailView.as_view(), name='detail'),
    path('edit/<int:pk>/', views.EventUpdateView.as_view(), name='edit'),
    path('delete/<int:pk>/', views.EventDeleteView.as_view(), name='delete'),
    path('code/<int:pk>/generate/', views.CodeFormView.as_view(), name='code'),
    path('<int:event_id>/action/', include('apps.action.urls')),
    path('<int:event_id>/product/', include('apps.product.urls')),
    path('<int:event_id>/participant/', include('apps.participant.urls')),
]
