from django import forms
from django.conf import settings
from apps.event.models import Participant, Event
from apps.product.models import Product
from apps.balance.models import Movement


class CreditForm(forms.ModelForm):
    participant = forms.ModelChoiceField(
        label='', queryset=Participant.objects.all(),
        widget=forms.HiddenInput)
    event = forms.ModelChoiceField(
        label='', queryset=Event.objects.all(),
        widget=forms.HiddenInput)

    class Meta:
        model = Movement
        fields = ['participant', 'event', 'status', 'value']
        widgets = {
            'participant': forms.HiddenInput,
            'event': forms.HiddenInput
        }
        fieldsets = (
            ('', {'fields': ('value',)}),
            ('', {'fields': ('status',)}),
            ('', {'fields': ('participant',)}),
            ('', {'fields': ('event',)}),
            ('', {'fields': ('kind',)}),
        )


class DebitForm(forms.ModelForm):
    participant = forms.ModelChoiceField(
        label='', queryset=Participant.objects.all(),
        widget=forms.HiddenInput)
    event = forms.ModelChoiceField(
        label='', queryset=Event.objects.all(),
        widget=forms.HiddenInput)
    product = forms.ModelChoiceField(
        label='', queryset=Product.objects.all(),
        widget=forms.HiddenInput)

    class Meta:
        model = Movement
        fields = ['participant', 'product', 'event', 'status', 'value']
        widgets = {
            'participant': forms.HiddenInput,
            'event': forms.HiddenInput
        }
        fieldsets = (
            ('', {'fields': ('value',)}),
            ('', {'fields': ('status',)}),
            ('', {'fields': ('participant',)}),
            ('', {'fields': ('event',)}),
        )
