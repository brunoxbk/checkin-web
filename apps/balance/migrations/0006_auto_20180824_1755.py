# Generated by Django 2.1 on 2018-08-24 20:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('balance', '0005_auto_20180824_1750'),
    ]

    operations = [
        migrations.AlterField(
            model_name='movement',
            name='participant',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='movement_participant', to='event.Participant', verbose_name='Participante'),
        ),
    ]
