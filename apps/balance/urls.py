from django.urls import path
from apps.balance import views

app_name = 'balance'

urlpatterns = [
    # path('', views.CreditListView.as_view(), name='list'),
    path(
        'addcredit/<int:participant_id>/<str:kind>/',
        views.CreditCreateView.as_view(), name='addcredit'),
    # path('detail/<int:pk>/', views.CreditDetailView.as_view(), name='detail'),
    # path('edit/<int:pk>/', views.CreditUpdateView.as_view(), name='edit'),
    # path('delete/<int:pk>/', views.CreditDeleteView.as_view(), name='delete'),
]
