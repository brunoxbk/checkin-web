# Create your views here.
from django.views.generic import ListView, CreateView, \
    DetailView, UpdateView, DeleteView
from apps.balance.models import Movement
from apps.event.models import Participant, Event
from apps.balance.forms import CreditForm, DebitForm
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy


class BalanceMixin(object):
    # @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(BalanceMixin, self)\
            .dispatch(request, *args, **kwargs)

    def get_initial(self):
        self.initial = {'event': self.kwargs.get('event_id')}
        return self.initial.copy()

    def get_context_data(self, **kwargs):
        context = super(BalanceMixin, self).get_context_data(**kwargs)

        event = get_object_or_404(Event, id=self.kwargs.get('event_id'))

        context.update(dict(event=event))
        return context


class CreditListView(ListView):
    model = Movement


class CreditCreateView(CreateView):
    model = Movement
    form_class = CreditForm

    def get_initial(self):
        self.initial = {
            'event': self.kwargs.get('event_id'),
            'kind': self.kwargs.get('kind'),
            'participant': self.kwargs.get('participant_id')}
        return self.initial.copy()


class CreditDetailView(DetailView):
    model = Movement


class CreditUpdateView(UpdateView):
    model = Movement
    form_class = CreditForm


class MovementDeleteView(DeleteView):
    model = Movement
    success_url = '/'
