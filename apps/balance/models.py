from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save
from django.urls import reverse
from django.dispatch import receiver


class Movement(models.Model):
    KIND_CHOICE = (
        ('', '---------'),
        ('D', 'Débito'),
        ('C', 'Crédito'),
    )
    participant = models.ForeignKey(
        'event.Participant', blank=True, null=True,
        verbose_name=_("Participante"),
        on_delete=models.DO_NOTHING,
        related_name='movement_participant')
    event = models.ForeignKey(
        'event.Event', blank=True, null=True, verbose_name=_("Evento"),
        on_delete=models.DO_NOTHING, related_name='credit_event')
    product = models.ForeignKey(
        'product.Product', blank=True, null=True, verbose_name=_("Produto"),
        on_delete=models.DO_NOTHING, related_name='product_event')
    status = models.BooleanField("Status", default=True)
    kind = models.CharField(
        _('Tipo'), max_length=2, choices=KIND_CHOICE,
        blank=True, null=True)
    value = models.DecimalField(
            "Valor", max_digits=8, decimal_places=2, default=0)
    created_at = models.DateTimeField("Created at", auto_now_add=True)
    updated_at = models.DateTimeField("Updated at", auto_now=True)

    def get_absolute_url(self):
        return reverse(
            'event:participant:list',
            kwargs={'event_id': self.event.id})

    class Meta:
        verbose_name = "Movimento"
        verbose_name_plural = "Movimentos"
        get_latest_by = 'created_at'
        ordering = ('-created_at',)
